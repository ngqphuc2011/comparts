module.exports = (sequelize, Sequelize) => {
  const pcBuild = sequelize.define(
    "pcBuild",
    {
      uuid: { type: Sequelize.STRING, primaryKey: true },
      name: { type: Sequelize.STRING },
      username: { type: Sequelize.STRING },
      total_price: { type: Sequelize.INTEGER },
      cpu_id: { type: Sequelize.INTEGER },
      cpu_quantity: { type: Sequelize.INTEGER },
      mobo_id: { type: Sequelize.INTEGER },
      mobo_quantity: { type: Sequelize.INTEGER },
      ram_id: { type: Sequelize.INTEGER },
      ram_quantity: { type: Sequelize.INTEGER },
      gpu_id: { type: Sequelize.INTEGER },
      gpu_quantity: { type: Sequelize.INTEGER },
      hdd_id: { type: Sequelize.INTEGER },
      hdd_quantity: { type: Sequelize.INTEGER },
      ssd_id: { type: Sequelize.INTEGER },
      ssd_quantity: { type: Sequelize.INTEGER },
      psu_id: { type: Sequelize.INTEGER },
      psu_quantity: { type: Sequelize.INTEGER },
      chas_id: { type: Sequelize.INTEGER },
      chas_quantity: { type: Sequelize.INTEGER },
    },
    {
      timestamps: false,
    }
  );
  return pcBuild;
};
