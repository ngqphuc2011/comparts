module.exports = (sequelize, Sequelize) => {
  const mobo = sequelize.define(
    "mobo",
    {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      name: { type: Sequelize.STRING },
      mfr: { type: Sequelize.STRING },
      chipset: { type: Sequelize.STRING },
      socket: { type: Sequelize.STRING },
      form_factor: { type: Sequelize.STRING },
      memory_capacity: { type: Sequelize.INTEGER },
      memory_type: { type: Sequelize.STRING },
      memory_freq: { type: Sequelize.INTEGER },
      ecc: { type: Sequelize.STRING },
      memory_slot_num: { type: Sequelize.INTEGER },
      pcie_x16_slot_num: { type: Sequelize.INTEGER },
      pcie_x8_slot_num: { type: Sequelize.INTEGER },
      pcie_x4_slot_num: { type: Sequelize.INTEGER },
      pcie_x2_slot_num: { type: Sequelize.INTEGER },
      pcie_x1_slot_num: { type: Sequelize.INTEGER },
      sata_slot_num: { type: Sequelize.INTEGER },
      m2_slot_num: { type: Sequelize.INTEGER },
      price: { type: Sequelize.INTEGER },
      img: { type: Sequelize.TEXT },
    },
    {
      timestamps: false,
    }
  );
  return mobo;
};
