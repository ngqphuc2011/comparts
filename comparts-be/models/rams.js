module.exports = (sequelize, Sequelize) => {
	const ram = sequelize.define(
		"ram",
		{
			id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
			name: { type: Sequelize.STRING },
			model: { type: Sequelize.STRING },
			mfr: { type: Sequelize.STRING },
			capacity: { type: Sequelize.INTEGER },
			modules: { type: Sequelize.INTEGER },
			ecc: { type: Sequelize.STRING },
			memory_type: { type: Sequelize.STRING },
			memory_freq: { type: Sequelize.INTEGER },
			cas_latency: { type: Sequelize.INTEGER },
			voltage: { type: Sequelize.FLOAT },
			price: { type: Sequelize.INTEGER },
			img: { type: Sequelize.TEXT },
		},
		{
			timestamps: false,
		},
	);
	return ram;
};
