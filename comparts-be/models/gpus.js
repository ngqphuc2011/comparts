module.exports = (sequelize, Sequelize) => {
  const gpu = sequelize.define(
    "gpu",
    {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      name: { type: Sequelize.STRING },
      mfr: { type: Sequelize.STRING },
      engine_mfr: { type: Sequelize.STRING },
      engine_name: { type: Sequelize.STRING },
      core_speed: { type: Sequelize.INTEGER },
      boost_speed: { type: Sequelize.INTEGER },
      memory_size: { type: Sequelize.INTEGER },
      memory_type: { type: Sequelize.STRING },
      memory_bw: { type: Sequelize.FLOAT },
      memory_interface: { type: Sequelize.INTEGER },
      cuda_core: { type: Sequelize.INTEGER },
      dp_port_num: { type: Sequelize.INTEGER },
      hdmi_port_num: { type: Sequelize.INTEGER },
      dvi_port_num: { type: Sequelize.INTEGER },
      vga_port_num: { type: Sequelize.INTEGER },
      tdp: { type: Sequelize.INTEGER },
      psu_wattage: { type: Sequelize.INTEGER },
      price: { type: Sequelize.INTEGER },
      img: { type: Sequelize.TEXT },
    },
    {
      timestamps: false,
    }
  );
  return gpu;
};
