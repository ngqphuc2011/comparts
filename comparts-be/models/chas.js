module.exports = (sequelize, Sequelize) => {
  const chas = sequelize.define(
    "chas",
    {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      name: { type: Sequelize.STRING },
      model: { type: Sequelize.STRING },
      mfr: { type: Sequelize.STRING },
      form_factor: { type: Sequelize.STRING },
      mobo_form_factor: { type: Sequelize.STRING },
      psu_form_factor: { type: Sequelize.STRING },
      hdd_slot_num: { type: Sequelize.INTEGER },
      ssd_slot_num: { type: Sequelize.INTEGER },
      fan_slot_num: { type: Sequelize.INTEGER },
      gpu_length: { type: Sequelize.INTEGER },
      psu_length: { type: Sequelize.INTEGER },
      cpu_cooler_height: { type: Sequelize.INTEGER },
      price: { type: Sequelize.INTEGER },
      img: { type: Sequelize.TEXT },
    },
    {
      timestamps: false,
    }
  );
  return chas;
};
