module.exports = (sequelize, Sequelize) => {
  const psu = sequelize.define(
    "psu",
    {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      name: { type: Sequelize.STRING },
      mfr: { type: Sequelize.STRING },
      form_factor: { type: Sequelize.STRING },
      fan_size: { type: Sequelize.INTEGER },
      length: { type: Sequelize.INTEGER },
      input_voltage_min: { type: Sequelize.INTEGER },
      input_voltage_max: { type: Sequelize.INTEGER },
      wattage: { type: Sequelize.INTEGER },
      standard: { type: Sequelize.STRING },
      modular: { type: Sequelize.STRING },
      price: { type: Sequelize.INTEGER },
      img: { type: Sequelize.TEXT },
    },
    {
      timestamps: false,
    }
  );
  return psu;
};
