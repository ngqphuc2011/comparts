module.exports = (sequelize, Sequelize) => {
  const cpu = sequelize.define(
    "cpu",
    {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      name: { type: Sequelize.STRING },
      mfr: { type: Sequelize.STRING },
      core_num: { type: Sequelize.INTEGER },
      thread_num: { type: Sequelize.INTEGER },
      base_freq: { type: Sequelize.FLOAT },
      turbo_freq: { type: Sequelize.FLOAT },
      cache: { type: Sequelize.FLOAT },
      socket: { type: Sequelize.STRING },
      tdp: { type: Sequelize.INTEGER },
      memory_type: { type: Sequelize.STRING },
      memory_freq: { type: Sequelize.INTEGER },
      ecc: { type: Sequelize.STRING },
      lithography: { type: Sequelize.INTEGER },
      graphics: { type: Sequelize.STRING },
      price: { type: Sequelize.INTEGER },
      img: { type: Sequelize.TEXT },
    },
    {
      timestamps: false,
    }
  );
  return cpu;
};
