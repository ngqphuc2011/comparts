module.exports = (sequelize, Sequelize) => {
  const hdd = sequelize.define(
    "hdd",
    {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      name: { type: Sequelize.STRING },
      model: { type: Sequelize.STRING },
      mfr: { type: Sequelize.STRING },
      capacity: { type: Sequelize.INTEGER },
      interface: { type: Sequelize.STRING },
      form_factor: { type: Sequelize.STRING },
      cache: { type: Sequelize.INTEGER },
      rpm: { type: Sequelize.INTEGER },
      price: { type: Sequelize.INTEGER },
      img: { type: Sequelize.TEXT },
    },
    {
      timestamps: false,
    }
  );
  return hdd;
};
