module.exports = (sequelize, Sequelize) => {
  const ssd = sequelize.define(
    "ssd",
    {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      name: { type: Sequelize.STRING },
      model: { type: Sequelize.STRING },
      mfr: { type: Sequelize.STRING },
      capacity: { type: Sequelize.INTEGER },
      interface: { type: Sequelize.STRING },
      form_factor: { type: Sequelize.STRING },
      read_speed: { type: Sequelize.INTEGER },
      write_speed: { type: Sequelize.INTEGER },
      price: { type: Sequelize.INTEGER },
      img: { type: Sequelize.TEXT },
    },
    {
      timestamps: false,
    }
  );
  return ssd;
};
