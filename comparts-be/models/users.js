module.exports = (sequelize, Sequelize) => {
  const user = sequelize.define(
    "user",
    {
      username: { type: Sequelize.STRING, primaryKey: true },
      password: { type: Sequelize.STRING },
      email: { type: Sequelize.STRING, unique: true },
      avatar: { type: Sequelize.STRING },
      role: { type: Sequelize.STRING },
    },
    {
      timestamps: false,
    }
  );
  return user;
};
