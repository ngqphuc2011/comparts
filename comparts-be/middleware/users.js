const sequelize = require("../config/database");
const { Sequelize } = require("sequelize");
const jwt = require("jsonwebtoken");
const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const user = require("../models/users")(sequelize, Sequelize);

module.exports = {
  checkDuplicateUsername: (req, res, next) => {
    user
      .findOne({
        where: {
          username: req.body.username,
        },
      })
      .then((result) => {
        if (result) {
          return res.status(409).send({
            msg: "This username is already in use",
          });
        } else {
          next();
        }
      });
  },
  checkDuplicateEmail: (req, res, next) => {
    user
      .findOne({
        where: {
          email: req.body.email,
        },
      })
      .then((result) => {
        if (result) {
          return res.status(409).send({
            msg: "This email is already in use",
          });
        } else {
          next();
        }
      });
  },
  validateRegister: (req, res, next) => {
    if (!req.body.username || req.body.username.length < 3) {
      return res.status(400).send({
        msg: "Please enter a username with min. 3 chars",
      });
    }
    if (!req.body.password || req.body.password.length < 6) {
      return res.status(400).send({
        msg: "Please enter a password with min. 6 chars",
      });
    }
    if (!regex.test(req.body.email)) {
      return res.status(400).send({
        msg: "Invalid email",
      });
    }
    if (
      !req.body.password_confirm ||
      req.body.password !== req.body.password_confirm
    ) {
      return res.status(400).send({
        msg: "Both passwords must match",
      });
    }
    next();
  },
  validateUpdate: (req, res, next) => {
    if (!regex.test(req.body.email)) {
      return res.status(400).send({
        msg: "Invalid email",
      });
    }
    next();
  },
  validateSetPassword: (req, res, next) => {
    if (!req.body.new_password || req.body.new_password.length < 6) {
      return res.status(400).send({
        msg: "Please enter a password with min. 6 chars",
      });
    }
    if (
      !req.body.password_confirm ||
      req.body.new_password !== req.body.password_confirm
    ) {
      return res.status(400).send({
        msg: "Both passwords must match",
      });
    }
    next();
  },
  isLoggedIn: (req, res, next) => {
    try {
      const token = req.get("Authorization").split(" ")[1];
      const decoded = jwt.verify(token, process.env.JWT_KEY);
      req.userData = decoded;
      next();
    } catch (err) {
      return res.status(401).send({
        msg: "Not Authorized",
      });
    }
  },
  isAuthUser: (req, res, next) => {
    try {
      const token = req.get("Authorization").split(" ")[1];
      const decoded = jwt.verify(token, process.env.JWT_KEY);
      req.userData = decoded;
      if (decoded.username === req.params.username) {
        next();
      } else {
        return res.status(403).send({
          msg: "No Permission",
        });
      }
    } catch (err) {
      return res.status(401).send({
        msg: "Not Authorized",
      });
    }
  },
  isAdmin: (req, res, next) => {
    try {
      const token = req.get("Authorization").split(" ")[1];
      const decoded = jwt.verify(token, process.env.JWT_KEY);
      req.userData = decoded;
      user.findByPk(decoded.username).then((user) => {
        if (user.role === "admin") {
          next();
        } else {
          return res.status(403).send({
            msg: "Require Admin Role",
          });
        }
      });
    } catch (err) {
      return res.status(401).send({
        msg: "Not Authorized",
      });
    }
  },
};
