var express = require("express");
var router = express.Router();
var cpu = require("../controllers/cpus");
var common = require("../controllers/common");
var userMiddleware = require("../middleware/users");

router.get("/", userMiddleware.isLoggedIn, cpu.search);
router.post("/", userMiddleware.isAdmin, cpu.create);
router.get("/:id(\\d+)", userMiddleware.isLoggedIn, cpu.detail);
router.delete("/:id(\\d+)", userMiddleware.isAdmin, cpu.delete);
router.put("/:id(\\d+)", userMiddleware.isAdmin, cpu.update);
router.post("/upload", userMiddleware.isAdmin, cpu.uploadImg);
router.post("/upload/bulk", userMiddleware.isAdmin, cpu.uploadBulkImg);
router.delete("/upload", userMiddleware.isAdmin, cpu.deleteImg);
router.post("/export/xlsx", userMiddleware.isAdmin, common.exportXlsx);
router.post("/export/csv", userMiddleware.isAdmin, common.exportCsv);
router.post("/import", userMiddleware.isAdmin, cpu.import);

module.exports = router;
