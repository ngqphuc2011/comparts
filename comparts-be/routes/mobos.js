var express = require("express");
var router = express.Router();
var mobo = require("../controllers/mobos");
var common = require("../controllers/common");
var userMiddleware = require("../middleware/users");

router.get("/", userMiddleware.isLoggedIn, mobo.search);
router.post("/", userMiddleware.isAdmin, mobo.create);
router.get("/:id(\\d+)", userMiddleware.isLoggedIn, mobo.detail);
router.delete("/:id(\\d+)", userMiddleware.isAdmin, mobo.delete);
router.put("/:id(\\d+)", userMiddleware.isAdmin, mobo.update);
router.post("/upload", userMiddleware.isAdmin, mobo.uploadImg);
router.post("/upload/bulk", userMiddleware.isAdmin, mobo.uploadBulkImg);
router.delete("/upload", userMiddleware.isAdmin, mobo.deleteImg);
router.post("/export/xlsx", userMiddleware.isAdmin, common.exportXlsx);
router.post("/export/csv", userMiddleware.isAdmin, common.exportCsv);
router.post("/import", userMiddleware.isAdmin, mobo.import);

module.exports = router;
