var express = require("express");
var router = express.Router();
var ram = require("../controllers/rams");
var common = require("../controllers/common");
var userMiddleware = require("../middleware/users");

router.get("/", userMiddleware.isLoggedIn, ram.search);
router.post("/", userMiddleware.isAdmin, ram.create);
router.get("/:id(\\d+)", userMiddleware.isLoggedIn, ram.detail);
router.delete("/:id(\\d+)", userMiddleware.isAdmin, ram.delete);
router.put("/:id(\\d+)", userMiddleware.isAdmin, ram.update);
router.post("/upload", userMiddleware.isAdmin, ram.uploadImg);
router.post("/upload/bulk", userMiddleware.isAdmin, ram.uploadBulkImg);
router.delete("/upload", userMiddleware.isAdmin, ram.deleteImg);
router.post("/export/xlsx", userMiddleware.isAdmin, common.exportXlsx);
router.post("/export/csv", userMiddleware.isAdmin, common.exportCsv);
router.post("/import", userMiddleware.isAdmin, ram.import);

module.exports = router;
