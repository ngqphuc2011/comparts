var express = require("express");
var router = express.Router();
var ssd = require("../controllers/ssds");
var common = require("../controllers/common");
var userMiddleware = require("../middleware/users");

router.get("/", userMiddleware.isLoggedIn, ssd.search);
router.post("/", userMiddleware.isAdmin, ssd.create);
router.get("/:id(\\d+)", userMiddleware.isLoggedIn, ssd.detail);
router.delete("/:id(\\d+)", userMiddleware.isAdmin, ssd.delete);
router.put("/:id(\\d+)", userMiddleware.isAdmin, ssd.update);
router.post("/upload", userMiddleware.isAdmin, ssd.uploadImg);
router.post("/upload/bulk", userMiddleware.isAdmin, ssd.uploadBulkImg);
router.delete("/upload", userMiddleware.isAdmin, ssd.deleteImg);
router.post("/export/xlsx", userMiddleware.isAdmin, common.exportXlsx);
router.post("/export/csv", userMiddleware.isAdmin, common.exportCsv);
router.post("/import", userMiddleware.isAdmin, ssd.import);

module.exports = router;
