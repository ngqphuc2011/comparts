var express = require("express");
var router = express.Router();
var psu = require("../controllers/psus");
var common = require("../controllers/common");
var userMiddleware = require("../middleware/users");

router.get("/", userMiddleware.isLoggedIn, psu.search);
router.post("/", userMiddleware.isAdmin, psu.create);
router.get("/:id(\\d+)", userMiddleware.isLoggedIn, psu.detail);
router.delete("/:id(\\d+)", userMiddleware.isAdmin, psu.delete);
router.put("/:id(\\d+)", userMiddleware.isAdmin, psu.update);
router.post("/upload", userMiddleware.isAdmin, psu.uploadImg);
router.post("/upload/bulk", userMiddleware.isAdmin, psu.uploadBulkImg);
router.delete("/upload", userMiddleware.isAdmin, psu.deleteImg);
router.post("/export/xlsx", userMiddleware.isAdmin, common.exportXlsx);
router.post("/export/csv", userMiddleware.isAdmin, common.exportCsv);
router.post("/import", userMiddleware.isAdmin, psu.import);

module.exports = router;
