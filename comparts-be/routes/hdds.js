var express = require("express");
var router = express.Router();
var hdd = require("../controllers/hdds");
var common = require("../controllers/common");
var userMiddleware = require("../middleware/users");

router.get("/", userMiddleware.isLoggedIn, hdd.search);
router.post("/", userMiddleware.isAdmin, hdd.create);
router.get("/:id(\\d+)", userMiddleware.isLoggedIn, hdd.detail);
router.delete("/:id(\\d+)", userMiddleware.isAdmin, hdd.delete);
router.put("/:id(\\d+)", userMiddleware.isAdmin, hdd.update);
router.post("/upload", userMiddleware.isAdmin, hdd.uploadImg);
router.post("/upload/bulk", userMiddleware.isAdmin, hdd.uploadBulkImg);
router.delete("/upload", userMiddleware.isAdmin, hdd.deleteImg);
router.post("/export/xlsx", userMiddleware.isAdmin, common.exportXlsx);
router.post("/export/csv", userMiddleware.isAdmin, common.exportCsv);
router.post("/import", userMiddleware.isAdmin, hdd.import);

module.exports = router;
