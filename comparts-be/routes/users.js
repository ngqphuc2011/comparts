const express = require("express");
const router = express.Router();
const userMiddleware = require("../middleware/users.js");
const user = require("../controllers/users");

router.post(
  "/register",
  userMiddleware.validateRegister,
  userMiddleware.checkDuplicateUsername,
  userMiddleware.checkDuplicateEmail,
  user.register
);
router.post("/login", user.login);
router.put(
  "/:username",
  userMiddleware.isLoggedIn,
  userMiddleware.validateUpdate,
  user.update
);
router.put(
  "/set-password/:username",
  userMiddleware.isLoggedIn,
  userMiddleware.validateSetPassword,
  user.setPassword
);
router.get("/:username", userMiddleware.isAuthUser, user.detail);
router.post("/upload", userMiddleware.isLoggedIn, user.uploadImg);
router.delete("/upload", userMiddleware.isLoggedIn, user.deleteImg);

module.exports = router;
