var express = require("express");
var router = express.Router();
var pcBuild = require("../controllers/pc-builds");
var userMiddleware = require("../middleware/users");

router.get("/", userMiddleware.isLoggedIn, pcBuild.search);
router.get("/:uuid", userMiddleware.isLoggedIn, pcBuild.detail);
router.post("/", userMiddleware.isLoggedIn, pcBuild.upsert);

module.exports = router;
