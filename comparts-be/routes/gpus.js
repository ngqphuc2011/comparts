var express = require("express");
var router = express.Router();
var gpu = require("../controllers/gpus");
var common = require("../controllers/common");
var userMiddleware = require("../middleware/users");

router.get("/", userMiddleware.isLoggedIn, gpu.search);
router.post("/", userMiddleware.isAdmin, gpu.create);
router.get("/:id(\\d+)", userMiddleware.isLoggedIn, gpu.detail);
router.delete("/:id(\\d+)", userMiddleware.isAdmin, gpu.delete);
router.put("/:id(\\d+)", userMiddleware.isAdmin, gpu.update);
router.post("/upload", userMiddleware.isAdmin, gpu.uploadImg);
router.post("/upload/bulk", userMiddleware.isAdmin, gpu.uploadBulkImg);
router.delete("/upload", userMiddleware.isAdmin, gpu.deleteImg);
router.post("/export/xlsx", userMiddleware.isAdmin, common.exportXlsx);
router.post("/export/csv", userMiddleware.isAdmin, common.exportCsv);
router.post("/import", userMiddleware.isAdmin, gpu.import);

module.exports = router;
