var express = require("express");
var router = express.Router();
var chas = require("../controllers/chas");
var common = require("../controllers/common");
var userMiddleware = require("../middleware/users");

router.get("/", userMiddleware.isLoggedIn, chas.search);
router.post("/", userMiddleware.isAdmin, chas.create);
router.get("/:id(\\d+)", userMiddleware.isLoggedIn, chas.detail);
router.delete("/:id(\\d+)", userMiddleware.isAdmin, chas.delete);
router.put("/:id(\\d+)", userMiddleware.isAdmin, chas.update);
router.post("/upload", userMiddleware.isAdmin, chas.uploadImg);
router.post("/upload/bulk", userMiddleware.isAdmin, chas.uploadBulkImg);
router.delete("/upload", userMiddleware.isAdmin, chas.deleteImg);
router.post("/export/xlsx", userMiddleware.isAdmin, common.exportXlsx);
router.post("/export/csv", userMiddleware.isAdmin, common.exportCsv);
router.post("/import", userMiddleware.isAdmin, chas.import);

module.exports = router;
