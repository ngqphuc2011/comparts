const sequelize = require("../config/database");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const fs = require("fs");
const publicFilePath = "./public/images/ssds";
const { upload, uploadBulk } = require("../utils/multer");
const csv = require("csvtojson");
const ssd = require("../models/ssds")(sequelize, Sequelize);
const { initSsd } = require("../utils/init");
initSsd(ssd);

module.exports = {
  search: (req, res) => {
    const getPagination = (page, size) => {
      const limit = size ? +size : 6;
      const offset = page ? page * limit : 0;

      return { limit, offset };
    };

    const getPagingData = (data, page, limit) => {
      const { count: totalItems, rows: items } = data;
      const currentPage = page ? +page : 0;
      const totalPages = Math.ceil(totalItems / limit);

      return { items, totalItems, totalPages, currentPage };
    };

    const reservedParams = ["page", "size", "sort", "order"];
    const likeParams = ["name"];
    const rangeParams = ["price"];

    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    let { sort, order } = req.query;

    if (!sort) {
      sort = "name";
    }
    if (!order) {
      order = "ASC";
    }

    let where = [];
    for (q in req.query) {
      let obj = {};
      if (likeParams.includes(q)) {
        obj[q] = { [Op.like]: `%${[req.query[q]]}%` };
        where.push(obj);
      } else if (rangeParams.includes(q)) {
        let range = JSON.parse(req.query[q]);
        obj[q] = { [Op.gte]: range.min, [Op.lte]: range.max };
        where.push(obj);
      } else if (!reservedParams.includes(q)) {
        if (req.query[q] instanceof Array) {
          obj[q] = { [Op.in]: req.query[q] };
        } else {
          if (req.query[q] !== "") {
            obj[q] = { [Op.in]: [req.query[q]] };
          }
        }
        where.push(obj);
      }
    }
    ssd
      .findAndCountAll({
        where: {
          [Op.and]: where,
        },
        limit,
        offset,
        order: [[sort, order]],
      })
      .then((ssd) => {
        const response = getPagingData(ssd, page, limit);
        res.send(response);
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  },
  detail: (req, res) => {
    ssd
      .findByPk(req.params.id)
      .then((ssd) => {
        res.json(ssd);
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  },
  delete: (req, res) => {
    ssd
      .destroy({ where: { id: req.params.id } })
      .then(() => {
        res.json("Deleted");
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  },
  create: (req, res) => {
    ssd
      .create(req.body)
      .then(() => {
        res.json("Created");
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  },
  update: (req, res) => {
    ssd
      .update(req.body, {
        where: {
          id: req.params.id,
        },
      })
      .then(() => {
        res.json("Updated");
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  },
  uploadImg: (req, res) => {
    upload(publicFilePath, "img")(req, res, (err) => {
      res.json(req.file);
    });
  },
  uploadBulkImg: (req, res) => {
    uploadBulk(publicFilePath, "img[]")(req, res, (err) => {
      res.json(req.file);
    });
  },
  deleteImg: (req, res) => {
    req.body.forEach((img) => {
      fs.unlink(`${publicFilePath}/${img}`, (err) => {
        res.json("Deleted");
      });
    });
  },
  import: (req, res) => {
    upload(`${publicFilePath}`, "csv")(req, res, (err) => {
      if (req.file) {
        const csvFilePath = `${publicFilePath}/${req.file.filename}`;
        csv()
          .fromFile(csvFilePath)
          .then((jsonObj) => {
            ssd
              .bulkCreate(jsonObj)
              .then(() => {
                res.json("Imported");
              })
              .catch((err) => {
                res.status(400).send(err);
              });
          })
          .finally(() => {
            fs.unlink(csvFilePath, (err) => {});
          });
      } else {
        res.status(500).send();
      }
    });
  },
};
