const sequelize = require("../config/database");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const pcBuild = require("../models/pc-builds")(sequelize, Sequelize);

module.exports = {
  search: (req, res) => {
    const getPagination = (page, size) => {
      const limit = size ? +size : 6;
      const offset = page ? page * limit : 0;

      return { limit, offset };
    };

    const getPagingData = (data, page, limit) => {
      const { count: totalItems, rows: items } = data;
      const currentPage = page ? +page : 0;
      const totalPages = Math.ceil(totalItems / limit);

      return { items, totalItems, totalPages, currentPage };
    };

    const reservedParams = ["page", "size", "sort", "order"];

    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    let { sort, order } = req.query;

    if (!sort) {
      sort = "name";
    }
    if (!order) {
      order = "ASC";
    }

    let where = [];
    for (q in req.query) {
      let obj = {};
      if (!reservedParams.includes(q)) {
        {
          obj[q] = req.query[q];
        }
        where.push(obj);
      }
    }

    pcBuild
      .findAndCountAll({
        where: {
          [Op.and]: where,
        },
        limit,
        offset,
        order: [[sort, order]],
      })
      .then((pcBuilds) => {
        const response = getPagingData(pcBuilds, page, limit);
        res.send(response);
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  },
  detail: (req, res) => {
    pcBuild
      .findByPk(req.params.uuid)
      .then((pcBuild) => {
        res.json(pcBuild);
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  },
  upsert: (req, res) => {
    pcBuild
      .findOne({
        where: {
          uuid: req.body.uuid,
        },
      })
      .then((result) => {
        if (!result || result.username === req.userData.username) {
          pcBuild
            .upsert(req.body)
            .then(() => {
              res.json("Updated");
            })
            .catch((err) => {
              res.status(400).send(err);
            });
        } else {
          res.status(403).send({
            msg: "No Permission",
          });
        }
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  },
};
