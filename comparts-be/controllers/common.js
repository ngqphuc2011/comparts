const XLSX = require("xlsx");
const { Parser } = require("json2csv");

module.exports = {
  exportXlsx: (req, res) => {
    let ws = XLSX.utils.json_to_sheet(req.body);
    let wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws);
    let wbbuf = XLSX.write(wb, { bookType: "xlsx", type: "buffer" });
    res.json(wbbuf);
  },
  exportCsv: (req, res) => {
    const json2csvParser = new Parser();
    const csv = json2csvParser.parse(req.body);
    res.json(csv);
  },
  getNullImage: (req, res) => {
    res.end();
  },
};
