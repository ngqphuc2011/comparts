const sequelize = require("../config/database");
const { Sequelize } = require("sequelize");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const fs = require("fs");
const publicFilePath = "./public/images/users";
const { upload } = require("../utils/multer");
const user = require("../models/users")(sequelize, Sequelize);
const { initUser } = require("../utils/init");
initUser(user);

module.exports = {
  register: (req, res) => {
    bcrypt.hash(req.body.password, 10, (err, hash) => {
      if (err) {
        return res.status(500).send({
          msg: err,
        });
      } else {
        user
          .create({
            username: req.body.username,
            password: hash,
            email: req.body.email,
            avatar: req.body.avatar,
            role: "user",
          })
          .then(() => {
            return res.status(201).send({
              msg: "Registered",
            });
          })
          .catch((err) => {
            return res.status(400).send({
              msg: err,
            });
          });
      }
    });
  },
  update: (req, res) => {
    user
      .update(req.body, {
        where: {
          username: req.params.username,
        },
      })
      .then(() => {
        res.json("Updated");
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  },
  detail: (req, res) => {
    user
      .findByPk(req.params.username)
      .then((user) => {
        res.json(user);
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  },
  uploadImg: (req, res) => {
    upload(publicFilePath, "img")(req, res, (err) => {
      res.json(req.file);
    });
  },
  deleteImg: (req, res) => {
    req.body.forEach((img) => {
      fs.unlink(`${publicFilePath}/${img}`, (err) => {
        res.json("Deleted");
      });
    });
  },
  setPassword: (req, res) => {
    user
      .findByPk(req.params.username)
      .then((user) => {
        bcrypt.compare(
          req.body.current_password,
          user.password,
          (bErr, bRes) => {
            if (bErr) {
              return res.status(500).send({
                msg: bErr,
              });
            }
            if (bRes) {
              bcrypt.hash(req.body.new_password, 10, (err, hash) => {
                if (err) {
                  return res.status(500).send({
                    msg: err,
                  });
                }
                user
                  .update(
                    {
                      password: hash,
                    },
                    {
                      where: {
                        username: req.params.username,
                      },
                    }
                  )
                  .then(() => {
                    return res.status(200).send({
                      msg: "Password is updated",
                    });
                  })
                  .catch((err) => {
                    return res.status(400).send({
                      msg: err,
                    });
                  });
              });
            } else {
              return res.status(400).send({
                msg: "Current password is incorrect",
              });
            }
          }
        );
      })
      .catch((err) => {
        return res.status(400).send({
          msg: err,
        });
      });
  },
  login: (req, res) => {
    user
      .findOne({
        where: {
          username: req.body.username,
        },
      })
      .then((result) => {
        if (!result) {
          return res.status(401).send({
            msg: "Username or password is incorrect",
          });
        }
        bcrypt.compare(req.body.password, result["password"], (bErr, bRes) => {
          if (bErr) {
            return res.status(401).send({
              msg: bErr,
            });
          }
          if (bRes) {
            const token = jwt.sign(
              {
                username: result.username,
                userId: result.id,
              },
              process.env.JWT_KEY,
              {
                expiresIn: "72h",
              }
            );
            return res.status(200).send({
              msg: "Logged in",
              token,
              user: result,
            });
          } else {
            return res.status(401).send({
              msg: "Username or password is incorrect",
            });
          }
        });
      })
      .catch((err) => {
        return res.status(400).send({
          msg: err,
        });
      });
  },
};
