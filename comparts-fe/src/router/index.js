import Vue from "vue";
import Router from "vue-router";
import store from "../store";
import Homepage from "../components/Homepage";
import CpuList from "../components/cpus/CpuList";
import MoboList from "../components/mobos/MoboList";
import GpuList from "../components/gpus/GpuList";
import RamList from "../components/rams/RamList";
import HddList from "../components/hdds/HddList";
import SsdList from "../components/ssds/SsdList";
import PsuList from "../components/psus/PsuList";
import ChasList from "../components/chas/ChasList";
import PcBuilder from "../components/pc-builds/PcBuilder";
import PcBuildList from "../components/pc-builds/PcBuildList";
import Login from "../components/users/Login";
import Register from "../components/users/Register";
import Profile from "../components/users/Profile";
import SetPassword from "../components/users/SetPassword";

Vue.use(Router);

const router = new Router({
  mode: "hash",
  routes: [
    {
      path: "/",
      name: "homepage",
      component: Homepage
    },
    {
      path: "/cpu",
      name: "cpu",
      component: CpuList
    },
    {
      path: "/mobo",
      name: "mobo",
      component: MoboList
    },
    {
      path: "/ram",
      name: "ram",
      component: RamList
    },
    {
      path: "/hdd",
      name: "hdd",
      component: HddList
    },
    {
      path: "/ssd",
      name: "ssd",
      component: SsdList
    },
    {
      path: "/gpu",
      name: "gpu",
      component: GpuList
    },
    {
      path: "/psu",
      name: "psu",
      component: PsuList
    },
    {
      path: "/chas",
      name: "chas",
      component: ChasList
    },
    {
      path: "/pc-builder/:id",
      name: "pc-builder",
      component: PcBuilder
    },
    {
      path: "/personal-build-list",
      name: "personal-build-list",
      component: PcBuildList,
      props: {
        mode: true
      }
    },
    {
      path: "/social-build-list",
      name: "social-build-list",
      component: PcBuildList,
      props: {
        mode: false
      }
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile
    },
    {
      path: "/set-password",
      name: "set-password",
      component: SetPassword
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLoggedIn) next({ name: "homepage" });
        else next();
      }
    },
    {
      path: "/register",
      name: "register",
      component: Register,
      beforeEnter: (to, from, next) => {
        if (store.getters.isLoggedIn) next({ name: "homepage" });
        else next();
      }
    },
    {
      path: "*",
      name: "not-found",
      redirect: { name: "homepage" }
    }
  ]
});
router.beforeEach((to, from, next) => {
  if (
    to.name !== "login" &&
    to.name !== "register" &&
    !store.getters.isLoggedIn
  ) {
    next({ name: "login" });
  } else next();
});

export default router;
