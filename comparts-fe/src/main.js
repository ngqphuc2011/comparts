// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import Vuetify from "vuetify";
import "@mdi/font/css/materialdesignicons.css";
import Axios from "axios";
import store from "./store";
import i18n from "./i18n";
import "./global.css";
import { saveAs } from "file-saver";

Vue.config.productionTip = false;
Vue.use(vuetify);
Vue.prototype.$http = Axios;
Axios.defaults.headers.common[
  "Authorization"
] = `Bearer ${store.state.user.token}`;
Axios.interceptors.response.use(
  response => {
    return response;
  },
  err => {
    if (err.response.status === 401) {
      store.dispatch("logout");
    }
    return Promise.reject(err);
  }
);
Vue.prototype.baseUrl = process.env.PATH;

/* eslint-disable no-new */
new Vue({
  el: "#app",
  i18n,
  router,
  store,
  saveAs,
  vuetify: new Vuetify(),
  components: { App },
  template: "<App/>"
});

export default new Vuetify({
  icons: {
    iconfont: "mdi" // default - only for display purposes
  }
});
