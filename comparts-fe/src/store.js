import Vue from "vue";
import Vuex from "vuex";
import i18n from "./i18n";
import Axios from "axios";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);
const initLang = () => {
  let lang = window.localStorage.getItem("language");
  return lang || "vi";
};

const getDefaultState = () => {
  return {
    token: "",
    user: {}
  };
};

const userModule = {
  strict: true,
  state: getDefaultState(),
  getters: {
    isLoggedIn: state => {
      return state.token;
    },
    getUsername: state => {
      return state.user.username;
    },
    getEmail: state => {
      return state.user.email;
    },
    getAvatar: state => {
      return state.user.avatar;
    },
    isAdmin: state => {
      return state.user.role === "admin";
    }
  },
  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;
    },
    SET_USER(state, user) {
      state.user = user;
    },
    RESET(state) {
      Object.assign(state, getDefaultState());
    }
  },
  actions: {
    login: ({ commit, dispatch }, { token, user }) => {
      commit("SET_TOKEN", token);
      commit("SET_USER", user);
      Axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    },
    logout: ({ commit }) => {
      commit("RESET", "");
    }
  }
};

const languageModule = {
  state: {
    lang: initLang()
  },
  mutations: {
    SET_LANG(state, payload) {
      window.localStorage.setItem("language", payload);
      i18n.locale = payload;
      state.lang = i18n.locale;
    }
  },
  actions: {
    setLang({ commit }, payload) {
      commit("SET_LANG", payload);
    }
  }
};

export default new Vuex.Store({
  modules: {
    user: userModule,
    language: languageModule
  },
  plugins: [createPersistedState()]
});
