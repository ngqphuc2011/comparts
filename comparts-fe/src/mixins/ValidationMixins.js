export default {
  data() {
    return {
      requiredRule: v =>
        !!v || v === false || this.$t("message.required_rule_msg"),
      emailRule: v => {
        let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(v) || this.$t("message.email_rule_msg");
      },
      arrayrequiredRule: v =>
        (v instanceof Array && v.length > 0) ||
        this.$t("message.required_rule_msg"),
      textRule: v =>
        v === "" ||
        v === null ||
        v.length <= 50 ||
        this.$t("message.text_rule_msg"),
      numberRule: v =>
        (v <= 65536 && v >= 0) ||
        v === null ||
        this.$t("message.number_rule_msg"),
      intRule: v =>
        (v <= 2147483647 && v >= 0) ||
        v === null ||
        this.$t("message.int_rule_msg"),
      usernameRule: v => v.length >= 3 || this.$t("message.username_rule_msg"),
      passwordRule: v => v.length >= 6 || this.$t("message.password_rule_msg")
    };
  },
  methods: {}
};
