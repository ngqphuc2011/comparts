export default {
  data() {
    return {
      fileTypeList: [
        { text: "XLSX", value: "xlsx" },
        { text: "CSV", value: "csv" }
      ],
      gpuMemoryTypeList: [
        "DDR3",
        "GDDR3",
        "GDDR5",
        "GDDR5X",
        "GDDR6",
        "GDDR6X",
        "HBM",
        "HBM2"
      ],
      ramMemoryTypeList: ["DDR2", "DDR3", "DDR4"],
      eccSupportedList: [
        {
          text: "ECC",
          value: "ecc"
        },
        {
          text: "Non-ECC",
          value: "non-ecc"
        }
      ],
      moboFormFactorList: ["E-ATX", "ATX", "mATX", "mITX", "DTX", "SSI-EEB"],
      hddFormFactorList: ["3.5-inch", "2.5-inch"],
      hddInterfaceList: ["SATA-II", "SATA-III"],
      ssdFormFactorList: [
        "M.2 2280",
        "M.2 2260",
        "M.2 2242",
        "M.2 2230",
        "2.5-inch",
        "U.2"
      ],
      ssdInterfaceList: ["SATA-II", "SATA-III", "PCIe 3.0 x4", "PCIe 4.0 x4"],
      psuFormFactorList: ["ATX", "Flex-ATX", "SFX", "SFX-L"],
      psuStandardList: [
        "80 Plus White",
        "80 Plus Bronze",
        "80 Plus Silver",
        "80 Plus Gold",
        "80 Plus Platinum",
        "80 Plus Titanium"
      ],
      psuModularList: ["Full Modular", "Semi Modular", "Non Modular"],
      chasFormFactorList: [
        "Full Tower",
        "Mid Tower",
        "Mini Tower",
        "Small Form Factor"
      ],
      cpuMfrList: ["AMD", "Intel"],
      moboMfrList: ["ASRock", "ASUS", "GIGABYTE", "MSI", "Intel", "Supermicro"],
      supportedSocketList: [
        "LGA1156",
        "LGA1155",
        "LGA1150",
        "LGA1151",
        "LGA1200",
        "FM2",
        "AM3",
        "AM3+",
        "AM4",
        "TR4",
        "sTRX4",
        "LGA1366",
        "LGA2011",
        "LGA2066",
        "LGA3647"
      ],
      moboChipsetList: [
        "H55",
        "H61",
        "B75",
        "H81",
        "B85",
        "Z87",
        "Z97",
        "H110",
        "B150",
        "Z170",
        "B250",
        "Z270",
        "H310",
        "B360",
        "Z370",
        "B365",
        "Z390",
        "H410",
        "B460",
        "Z490",
        "A320",
        "B350",
        "B450",
        "B550",
        "X370",
        "X470",
        "X570",
        "X399",
        "TRX40",
        "X99",
        "X299",
        "C246",
        "C232",
        "C242",
        "C602",
        "C612",
        "C621",
        "C622"
      ],
      gpuMfrList: [
        "AMD",
        "ASUS",
        "EVGA",
        "GALAX",
        "GIGABYTE",
        "Inno3D",
        "MSI",
        "Palit",
        "SAPPHIRE",
        "ZOTAC"
      ],
      gpuEngineMfrList: ["AMD", "NVIDIA"],
      ramMfrList: [
        "ADATA",
        "Antec",
        "AVEXIR",
        "Corsair",
        "GIGABYTE",
        "G.SKILL",
        "KINGMAX",
        "Kingston",
        "OCPC",
        "TeamGroup"
      ],
      hddMfrList: ["Seagate", "HGST", "Toshiba", "Western Digital"],
      ssdMfrList: [
        "ADATA",
        "Apacer",
        "Corsair",
        "Crucial",
        "GIGABYTE",
        "HP",
        "Intel",
        "KINGMAX",
        "Kingston",
        "PLEXTOR",
        "Samsung",
        "Seagate",
        "Silicon Power",
        "TeamGroup"
      ],
      psuMfrList: [
        "AcBel",
        "AeroCool",
        "Antec",
        "ASUS",
        "Cooler Master",
        "Corsair",
        "FSP",
        "GIGABYTE",
        "Seasonic",
        "Super Flower",
        "Thermaltek",
        "Xigmatek"
      ],
      chasMfrList: [
        "AeroCool",
        "Antec",
        "ASUS",
        "Cooler Master",
        "COUGAR",
        "Deepcool",
        "GIGABYTE",
        "ID-COOLING",
        "JONSBO",
        "Lian Li",
        "MSI",
        "NZXT",
        "SAMA",
        "Thermaltek",
        "Xigmatek"
      ],

      //Floating buttons
      fab: false,

      //Expansion panel
      expansionPanel: "",

      //Search form
      showSearchForm: false,

      //Export form
      showExportForm: false,
      selectedExportFileType: "csv",

      //Import form
      showImportForm: false,
      selectedImportFile: null,
      selectedImportImages: [],
      importFileKey: 0
    };
  },
  methods: {
    toTopPage() {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    },
    compareObjects(a, b) {
      return JSON.stringify(a) === JSON.stringify(b);
    },
    formatNumber(num) {
      return num
        ? num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
        : "0";
    },
    toArrayBuffer(buf) {
      var ab = new ArrayBuffer(buf.length);
      var view = new Uint8Array(ab);
      for (var i = 0; i < buf.length; ++i) {
        view[i] = buf[i];
      }
      return ab;
    },
    async exportFile(url, arr, fileName, fileType) {
      switch (fileType) {
        case "xlsx":
          return this.$http.post(`${url}/${fileType}`, arr).then(res => {
            let buf = res.data.data;
            let arrBuf = this.toArrayBuffer(buf);
            let file = new Blob([arrBuf], {
              type: "application/octet-stream"
            });
            saveAs(file, `${fileName}-${Date.now()}.xlsx`);
          });
        case "csv":
          return this.$http.post(`${url}/${fileType}`, arr).then(res => {
            let file = new Blob([res.data], {
              type: "text/csv"
            });
            saveAs(file, `${fileName}-${Date.now()}.csv`);
          });
      }
    }
  }
};
