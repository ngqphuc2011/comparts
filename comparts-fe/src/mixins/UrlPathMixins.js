export default {
  data() {
    return {
      url: {
        login: `${this.baseUrl}/users/login`,
        register: `${this.baseUrl}/users/register`,
        user: `${this.baseUrl}/users`,
        userAvatar: `${this.baseUrl}/images/users`,
        userUploadAvatar: `${this.baseUrl}/users/upload`,
        userSetPassword: `${this.baseUrl}/users/set-password`,
        image: `${this.baseUrl}/images`,

        cpu: `${this.baseUrl}/cpus`,
        cpuImg: `${this.baseUrl}/images/cpus`,
        cpuUploadImg: `${this.baseUrl}/cpus/upload`,
        cpuUploadBulkImg: `${this.baseUrl}/cpus/upload/bulk`,
        cpuExport: `${this.baseUrl}/cpus/export`,
        cpuImport: `${this.baseUrl}/cpus/import`,

        mobo: `${this.baseUrl}/mobos`,
        moboImg: `${this.baseUrl}/images/mobos`,
        moboUploadImg: `${this.baseUrl}/mobos/upload`,
        moboUploadBulkImg: `${this.baseUrl}/mobos/upload/bulk`,
        moboExport: `${this.baseUrl}/mobos/export`,
        moboImport: `${this.baseUrl}/mobos/import`,

        gpu: `${this.baseUrl}/gpus`,
        gpuImg: `${this.baseUrl}/images/gpus`,
        gpuUploadImg: `${this.baseUrl}/gpus/upload`,
        gpuUploadBulkImg: `${this.baseUrl}/gpus/upload/bulk`,
        gpuExport: `${this.baseUrl}/gpus/export`,
        gpuImport: `${this.baseUrl}/gpus/import`,

        ram: `${this.baseUrl}/rams`,
        ramImg: `${this.baseUrl}/images/rams`,
        ramUploadImg: `${this.baseUrl}/rams/upload`,
        ramUploadBulkImg: `${this.baseUrl}/rams/upload/bulk`,
        ramExport: `${this.baseUrl}/rams/export`,
        ramImport: `${this.baseUrl}/rams/import`,

        hdd: `${this.baseUrl}/hdds`,
        hddImg: `${this.baseUrl}/images/hdds`,
        hddUploadImg: `${this.baseUrl}/hdds/upload`,
        hddUploadBulkImg: `${this.baseUrl}/hdds/upload/bulk`,
        hddExport: `${this.baseUrl}/hdds/export`,
        hddImport: `${this.baseUrl}/hdds/import`,

        ssd: `${this.baseUrl}/ssds`,
        ssdImg: `${this.baseUrl}/images/ssds`,
        ssdUploadImg: `${this.baseUrl}/ssds/upload`,
        ssdUploadBulkImg: `${this.baseUrl}/ssds/upload/bulk`,
        ssdExport: `${this.baseUrl}/ssds/export`,
        ssdImport: `${this.baseUrl}/ssds/import`,

        psu: `${this.baseUrl}/psus`,
        psuImg: `${this.baseUrl}/images/psus`,
        psuUploadImg: `${this.baseUrl}/psus/upload`,
        psuUploadBulkImg: `${this.baseUrl}/psus/upload/bulk`,
        psuExport: `${this.baseUrl}/psus/export`,
        psuImport: `${this.baseUrl}/psus/import`,

        chas: `${this.baseUrl}/chas`,
        chasImg: `${this.baseUrl}/images/chas`,
        chasUploadImg: `${this.baseUrl}/chas/upload`,
        chasUploadBulkImg: `${this.baseUrl}/chas/upload/bulk`,
        chasExport: `${this.baseUrl}/chas/export`,
        chasImport: `${this.baseUrl}/chas/import`,

        pcBuild: `${this.baseUrl}/pc-builds`
      }
    };
  }
};
